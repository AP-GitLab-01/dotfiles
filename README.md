# MartinBuildsFromSource Dotfiles

## About the repository

In this GitLab repositroy you can find my personal configurations of various tiling window manager, text editor, terminal emulators and etc. You are free to download this repositroy and use it as you wish. If you whant to know more about me and learn more about programming and Linux, check out page below.

# Tiling Window Manager Configs

- [DWM][dwm]
- [Qtile][qtile]

# Suckless Software Configs

- [DWM][dwm]
- [ST][st]
- [Dmenu][dmenu]
- [SlStatus][slstatus]

# Text Editor Configs

- [Neovim][nvim]

# Other Configs

- [Alacritty][alacritty]
- [Fish][fish]
- [Kitty][kitty]
- [Neofetch][neofetch]
- [Picom][picom]
- [Rofi][rofi]
- [Starship prompt][starship]

# How To Download My Dotfiles

Copy and paste the git command in your terminal.


```sh
git clone https://gitlab.com/mbfs/dotfiles
```

# About Me

I am Martin Brcković, the creator of the [MartinBuildsFromSource] [youtube] YouTube channel. I am a programming hobbyist and Linux enthusiast. I started my channel in January, 2022. with the purpose of sharing my programming and Linux journey.
In my videos you can also see me configuring all of the thing in this git repository. I also do Linux and programming videos in various topics. If you want to learn more about programming and Linux, check out the linux to my videos!

#MartinBuildsFromSource Links

## Watch MartinBuildsFromSource Videos

I upload my videos on two separate platforms, YouTube and Odysee/LBRY. So you can watch my content on either of those two pages.
- [YouTube][youtube]
- [Odysee/LBRY][odysee]

## License

Everything in this repository is under the MIT Licence. The MIT Licence is a permissive licence which means you can use,copy, modify, merge, publish, distribute, sublicence, and/or sell copies of the content of this repositroy.
The only rewuirement with the MIT License that the license and copyright notice must be provided with the software.

   [youtube]: <https://www.youtube.com/c/MartinBuildsFromSource>
   [odysee]: <https://odysee.com/@MartinBuildsFromSource:1>
   [alacritty]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/alacritty>
   [dmenu]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/dmenu>
   [dwm]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/dwm>
   [fish]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/fish>
   [kitty]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/kitty>
   [neofetch]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/neofetch>
   [nvim]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/nvim>
   [picom]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/picom>
   [qtile]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/qtile>
   [rofi]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/rofi>
   [slstatus]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/slstatus>
   [st]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config/st>
   [starship]: <https://gitlab.com/mbfs/dotfiles/-/tree/main/.config>

