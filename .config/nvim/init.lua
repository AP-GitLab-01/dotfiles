-- General functionallity and apperence
require "mbfsNeovim.options"
require "mbfsNeovim.keybindings"
require "mbfsNeovim.plugins"
require "mbfsNeovim.lualine"
require "mbfsNeovim.colorschemes"
require "mbfsNeovim.telescope"
require "mbfsNeovim.alpha"
require "mbfsNeovim.nvim-tree"
require "mbfsNeovim.bufferline"

-- Completion engine and Leungage server
require "mbfsNeovim.cmp"
require "mbfsNeovim.lsp"

-- Programming
require "mbfsNeovim.autopairs"
require "mbfsNeovim.treesitter"
require "mbfsNeovim.gitsigns"
require "mbfsNeovim.comments"
require "mbfsNeovim.toggleterm"







