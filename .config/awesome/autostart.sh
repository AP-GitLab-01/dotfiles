#!/usr/bin/env bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

#~/.fehbg &
run nm-applet
run xrandr --output HDMI-1-1 --mode 1920x1080 --output eDP-1 --mode 1920x1080 --left-of HDMI-1-1 &
run nitrogen --restore &
